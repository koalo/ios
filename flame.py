import machine

class Flame():
    def __init__(self,pinid):
        self.pin = machine.Pin(pinid)

    def burns(self):
        return self.pin.value() == 0

