from machine import Pin
import onewire
import time
import ds18x20

class OWTemp():
    def __init__(self,pinid):
        self.pin = Pin(pinid)
        self.ow = onewire.OneWire(self.pin)
        self.ds = ds18x20.DS18X20(self.ow)

    def scan(self):
        return self.ds.scan()

    def read(self,rom):
        self.ds.convert_temp()
        time.sleep_ms(75)
        return self.ds.read_temp(rom)

