import time
import mqtt
from flame import Flame
from display import Display
from machine import Timer
from controller import Controller
from temperatures import Temperatures
from gas import Gas

last_message = 0
message_interval = 5
counter = 0

display = Display()
mq = mqtt.MQTT(display)
controller = Controller()
controller.set_target(100)
temperatures = Temperatures()
flame = Flame(5)
gas = Gas()

current_bottom = temperatures.bottom()
current_top = temperatures.top()
ratio = 0

for i in range(20):
    display.set_status('Control in %i'%(20-i))
    time.sleep(1)

def control_step(_):
    global temperatures,controller,current_top,current_bottom,ratio

    current_bottom = temperatures.bottom()
    current_top = temperatures.top()

    u = controller.step(current_bottom)
    ratio = min(max(0.0,u),1.0)
    display.set_status('%.1f %i%%'%(current_bottom,int(ratio*100)))
    gas.set_ratio(ratio)

tim = Timer(-1)
tim.init(period=1000, mode=Timer.PERIODIC, callback=control_step)

while True:
    mq.publish(mqtt.CONTROLLER_KP, controller.get_Kp())
    mq.publish(mqtt.CONTROLLER_KI, controller.get_Ki())
    mq.publish(mqtt.CONTROLLER_KD, controller.get_Kd())
    mq.publish(mqtt.CONTROLLER_AW, controller.get_Aw())
    mq.publish(mqtt.CONTROLLER_E, controller.get_e())
    mq.publish(mqtt.CONTROLLER_ESUM, controller.get_esum())
    mq.publish(mqtt.CONTROLLER_EDIFF, controller.get_ediff())
    mq.publish(mqtt.CONTROLLER_U, controller.get_u())
    mq.publish(mqtt.CONTROLLER_TARGET, controller.get_target())
    mq.publish(mqtt.CONTROLLER_Y, controller.get_y())

    mq.publish(mqtt.GAS_RATIO, ratio)
    mq.publish(mqtt.TEMPERATURE_MEAT_TOP, temperatures.meat_top())
    mq.publish(mqtt.TEMPERATURE_MEAT_BOTTOM, temperatures.meat_bottom())
    mq.publish(mqtt.TEMPERATURE_TOP, current_top)
    mq.publish(mqtt.TEMPERATURE_BOTTOM, current_bottom)
    mq.publish(mqtt.FLAME, flame.burns())

    time.sleep(5)

