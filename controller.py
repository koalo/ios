class Controller:
    def __init__(self):
        self.Kp = 0.135
        self.Ki = 0.00038927
	self.Kd = 0
        self.Aw = 1200
        self.elast = 0
        self.target = 0
        self.esum = 0
        self.current = 0
        self.e = 0
        self.esum = 0
        self.ediff = 0
        self.u = 0

    def step(self,current):
        self.current = current
        self.e = self.target - self.current
        self.esum = max(min(self.esum + self.e, self.Aw), -self.Aw)
        self.ediff = self.elast-self.e
        self.u = self.Kp*self.e + self.Ki*self.esum + self.Kd*self.ediff
        self.elast = self.e
        return self.u

    def get_y(self):
        return self.current

    def get_Kp(self):
        return self.Kp

    def get_Ki(self):
        return self.Ki

    def get_Kd(self):
        return self.Kd

    def get_Aw(self):
        return self.Aw

    def get_e(self):
        return self.e

    def get_esum(self):
        return self.esum

    def get_ediff(self):
        return self.ediff

    def get_u(self):
        return self.u

    def get_target(self):
        return self.target

    def set_target(self,target):
        self.target = target

    def set_Kp(self,Kp):
        self.Kp = Kp

    def set_Ki(self,Ki):
        self.Ki = Ki

    def set_Kd(self,Kd):
        self.Kd = Kd

    def set_Aw(self,Aw):
        self.Aw = Aw

