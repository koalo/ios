from machine import Pin, SPI
import time
import math

class SPITemp():
    def __init__(self):
        # Hardware SPI
        self.spi = SPI(1, sck=Pin(18), mosi=Pin(23), miso=Pin(19))
        self.spi.init(baudrate=5000000)

    def read(self,cspin,max6675=False):
        cspin.on()
        time.sleep_ms(100)
        cspin.off()
    
        if not max6675:
            data = self.spi.read(4)
            data = int.from_bytes(data,"big")
            temp = (data >> 18)/4.0
        else:
            data = self.spi.read(2)
            data = int.from_bytes(data,"big")
            temp = (data >> 3)/4.0

        if temp > 1000:
            temp = float('NaN')

        cspin.on()
        return temp

