from umqtt.robust import MQTTClient
import ubinascii
import machine
import _thread
import time
import wifi

prefix = 'home/ios/'

TEMPERATURE_MEAT_TOP = prefix+'temperature/meat/top'
TEMPERATURE_MEAT_BOTTOM = prefix+'temperature/meat/bottom'
TEMPERATURE_TOP = prefix+'temperature/top'
TEMPERATURE_BOTTOM = prefix+'temperature/bottom'

CONTROLLER_KP = prefix+'controller/kp'
CONTROLLER_KI = prefix+'controller/ki'
CONTROLLER_KD = prefix+'controller/kd'
CONTROLLER_AW = prefix+'controller/aw'
CONTROLLER_E = prefix+'controller/e'
CONTROLLER_ESUM = prefix+'controller/esum'
CONTROLLER_EDIFF = prefix+'controller/ediff'
CONTROLLER_U = prefix+'controller/u'
CONTROLLER_TARGET = prefix+'controller/target'
CONTROLLER_Y = prefix+'controller/y'

FLAME = prefix+'flame'

GAS_RATIO = prefix+'gas/ratio'

class MQTT:
    def __init__(self,display):
        self.display = display
        self.connected = False
        self.lock = _thread.allocate_lock()
        _thread.start_new_thread(self.maintain_connection, ())

    def maintain_connection(self):
        i = 0
        wifi.connect(self.display)
        self.connected = False
        while True:
            time.sleep(4)

            i += 1
            if not self.lock.acquire(0):
                print("lk mc")
                continue

            try:
                if not self.connected:
                    mqtt_server = '151.217.19.69'
                    client_id = ubinascii.hexlify(machine.unique_id())
                    self.client = MQTTClient(client_id, mqtt_server)
                    self.client.connect()
                    self.connected = True
                self.client.ping()
            except OSError as e:
                self.connected = False

            self.lock.release()

            if self.connected:
                msg = "MQTT connected"
            else:
                msg = "MQTT unconnected"
            if i%2 == 0:
                msg += " ."
            self.display.set_network_status(msg)

    def publish(self,topic,val):
        if not self.lock.acquire(0):
            print("lk p")
            return

        try:
            if self.connected:
                self.client.publish(topic,str(val))
            else:
                print("nc")
        except OSError as e:
            self.connected = False

        self.lock.release()

