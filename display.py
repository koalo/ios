from machine import Pin, I2C
import ssd1306

class Display:
    def __init__(self):
        self.i2c = I2C(sda=Pin(4), scl=Pin(15))
        self.i2c.scan()
        self.dispon = Pin(16, Pin.OUT)
        self.dispon.on()
        oled_width = 128
        oled_height = 64
        self.oled = ssd1306.SSD1306_I2C(oled_width, oled_height, self.i2c)
        self.status = ''
        self.network_status = ''

    def update(self):
        self.oled.fill(0)
        self.oled.text(self.status, 0, 0)
        self.oled.text(self.network_status, 0, 40)
        self.oled.show()

    def set_status(self,text):
        self.status = text
        self.update()

    def set_network_status(self,text):
        self.network_status = text
        self.update()

