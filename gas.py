from machine import Pin,PWM
 
class Gas:
    def __init__(self):
        self.pwm = PWM(Pin(12))
        self.pwm.freq(50)

    def set_duty(self,duty):
        self.pwm.duty(duty)

    def set_ratio(self,ratio):
        ratio = min(max(0.0,ratio),1.0)
        ratio = 1-ratio # is inverted
        minduty = 50.0
        maxduty = 100.0
        self.pwm.duty(int(minduty+(maxduty-minduty)*ratio))

