import network
import time

def connect(disp):
    ssid = 'Camp2019-things'
    password = 'camp2019'

    station = network.WLAN(network.STA_IF)

    station.active(True)
    print(station.scan())
    station.connect(ssid, password)

    i = 0
    while station.isconnected() == False:
        time.sleep(1)
        i += 1
        msg = 'WiFi connecting'
        if i % 2 == 0:
            msg += '.'
        disp.set_network_status(msg)


    disp.set_network_status('WiFi connected')
    print(station.ifconfig())

