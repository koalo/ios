from owtemp import OWTemp
from spitemp import SPITemp
from machine import Pin

class Temperatures:
    def __init__(self):
        self.spitemp = SPITemp()
        self.owtemp = OWTemp(27)
        roms = sorted(self.owtemp.scan(),key=lambda x:int.from_bytes(x,"big"))
        print(roms)
        self.bottorom = None
        self.toprom = None
        if len(roms) == 2:
            self.toprom = roms[0]
            self.bottomrom = roms[1]

        self.meatbottompin = Pin(13, Pin.OUT)
        self.meattoppin = Pin(14, Pin.OUT)

    def meat_bottom(self):
        return self.spitemp.read(self.meatbottompin)

    def meat_top(self):
        return self.spitemp.read(self.meattoppin,True)

    def bottom(self):
        if self.bottomrom is None:
            return 0
        return self.owtemp.read(self.bottomrom)

    def top(self):
        if self.toprom is None:
            return 0
        return self.owtemp.read(self.toprom) 

