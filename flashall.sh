#!/bin/bash
PORT=/dev/ttyUSB0
FILES="\
	main.py
	spitemp.py\
	flame.py\
	gas.py\
	wifi.py\
	mqtt.py\
	owtemp.py\
	display.py\
	controller.py\
	temperatures.py\
	"
	#micropython-adafruit-ssd1306/ssd1306.py\

for f in $FILES;
do
	echo "Flashing ${f}"
	ampy --port ${PORT} put ${f}
done
